/* GStreamer Wayland video sink
 *
 * Copyright (C) 2014 Collabora Ltd.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "wldisplay-wlwindow-wlbuffer.h"

#include <wayland-client-protocol.h>
#include "wayland-drm-client-protocol.h"
#include <linux/input.h>
#include <omap_drm.h>
#include <omap_drmif.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

GST_DEBUG_CATEGORY_EXTERN (gstwayland_debug);
#define GST_CAT_DEFAULT gstwayland_debug

G_DEFINE_TYPE (GstWlDisplay, gst_wl_display, G_TYPE_OBJECT);

static void gst_wl_display_finalize (GObject * gobject);
static void input_grab (struct input *input, GstWlWindow *window);
static void input_ungrab (struct input *input);

static void
gst_wl_display_class_init (GstWlDisplayClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  gobject_class->finalize = gst_wl_display_finalize;
}

static void
gst_wl_display_init (GstWlDisplay * self)
{
  self->formats = g_array_new (FALSE, FALSE, sizeof (uint32_t));
  self->fd = -1;
  self->use_drm = FALSE;
  self->wl_fd_poll = gst_poll_new (TRUE);
  self->buffers = g_hash_table_new (g_direct_hash, g_direct_equal);
  g_mutex_init (&self->buffers_mutex);
}

static void
input_grab (struct input *input, GstWlWindow *window)
{
  input->grab = window;
}

static void
input_ungrab (struct input *input)
{
  input->grab = NULL;
}

static void
input_remove_pointer_focus (struct input *input)
{
  GstWlWindow *window = input->pointer_focus;

  if (!window)
    return;

  input->pointer_focus = NULL;
}

static void
input_destroy (struct input *input)
{
  input_remove_pointer_focus (input);

  if (input->display->seat_version >= 3) {
    if (input->pointer)
      wl_pointer_release (input->pointer);
  }

  wl_list_remove (&input->link);
  wl_seat_destroy (input->seat);
  free (input);
}

static void
display_destroy_inputs (GstWlDisplay *display)
{
  struct input *tmp;
  struct input *input;

  wl_list_for_each_safe (input, tmp, &display->input_list, link)
      input_destroy (input);
}

static void
gst_wl_display_finalize (GObject * gobject)
{
  GstWlDisplay *self = GST_WL_DISPLAY (gobject);

  gst_poll_set_flushing (self->wl_fd_poll, TRUE);
  g_thread_join (self->thread);

  /* to avoid buffers being unregistered from another thread
   * at the same time, take their ownership */
  g_mutex_lock (&self->buffers_mutex);
  self->shutting_down = TRUE;
  g_hash_table_foreach (self->buffers, (GHFunc) g_object_ref, NULL);
  g_mutex_unlock (&self->buffers_mutex);

  g_hash_table_foreach (self->buffers,
      (GHFunc) gst_wl_buffer_force_release_and_unref, NULL);
  g_hash_table_remove_all (self->buffers);

  g_array_unref (self->formats);
  
  if (self->dev) {
    omap_device_del (self->dev);
    self->dev = NULL;
  }
  if (self->fd !=-1)
    close (self->fd);

  gst_poll_free (self->wl_fd_poll);
  g_hash_table_unref (self->buffers);
  g_mutex_clear (&self->buffers_mutex);

  display_destroy_inputs (self);

  if (self->shm)
    wl_shm_destroy (self->shm);

  if (self->drm)
    wl_drm_destroy (self->drm);

  if (self->shell)
    wl_shell_destroy (self->shell);

  if (self->compositor)
    wl_compositor_destroy (self->compositor);

  if (self->subcompositor)
    wl_subcompositor_destroy (self->subcompositor);

  if (self->registry)
    wl_registry_destroy (self->registry);

  if (self->queue)
    wl_event_queue_destroy (self->queue);

  if (self->own_display) {
    wl_display_flush (self->display);
    wl_display_disconnect (self->display);
  }

  G_OBJECT_CLASS (gst_wl_display_parent_class)->finalize (gobject);
}

static void
sync_callback (void *data, struct wl_callback *callback, uint32_t serial)
{
  gboolean *done = data;
  *done = TRUE;
}

static const struct wl_callback_listener sync_listener = {
  sync_callback
};

static gint
gst_wl_display_roundtrip (GstWlDisplay * self)
{
  struct wl_callback *callback;
  gint ret = 0;
  gboolean done = FALSE;

  g_return_val_if_fail (self != NULL, -1);

  /* We don't own the display, process only our queue */
  callback = wl_display_sync (self->display);
  wl_callback_add_listener (callback, &sync_listener, &done);
  wl_proxy_set_queue ((struct wl_proxy *) callback, self->queue);
  while (ret != -1 && !done)
    ret = wl_display_dispatch_queue (self->display, self->queue);
  wl_callback_destroy (callback);

  return ret;
}

static void
shm_format (void *data, struct wl_shm *wl_shm, uint32_t format)
{
  GstWlDisplay *self = data;
  g_array_append_val (self->formats, format);
  GST_DEBUG ("shm got format: %" GST_FOURCC_FORMAT, GST_FOURCC_ARGS (format));
}

static const struct wl_shm_listener shm_listener = {
  shm_format
};

/* For wl_drm_listener */

static void
drm_handle_device (void *data, struct wl_drm *drm, const char *device)
{
  GstWlDisplay *d = data;
  drm_magic_t magic;
  d->fd = open (device, O_RDWR | O_CLOEXEC);
  if (d->fd == -1) {
    GST_ERROR ("could not open %s: %m", device);
    return;
  }
  drmGetMagic (d->fd, &magic);
  wl_drm_authenticate (d->drm, magic);
}

static void
drm_handle_format (void *data, struct wl_drm *drm, uint32_t format)
{
  GstWlDisplay *self = data;
  g_array_append_val (self->formats, format);
  GST_DEBUG ("drm got format: %" GST_FOURCC_FORMAT, GST_FOURCC_ARGS (format));
}

static void
drm_handle_authenticated (void *data, struct wl_drm *drm)
{
  GstWlDisplay *d = data;
  GST_DEBUG ("authenticated");
  d->dev = omap_device_new (d->fd);
  d->authenticated = 1;
  GST_DEBUG ("drm_handle_authenticated: dev: %p, d->authenticated: %d\n",
      d->dev, d->authenticated);
}

static const struct wl_drm_listener drm_listener = {
  drm_handle_device,
  drm_handle_format,
  drm_handle_authenticated
};


static void
pointer_handle_enter (void *data, struct wl_pointer *pointer,
    uint32_t serial, struct wl_surface *surface,
    wl_fixed_t sx_w, wl_fixed_t sy_w)
{
  struct input *input = data;

  if (!surface) {
    /* enter event for a window we've just destroyed */
    return;
  }

  input->display->serial = serial;
  input->pointer_focus = wl_surface_get_user_data (surface);
}

static void
pointer_handle_leave (void *data, struct wl_pointer *pointer,
    uint32_t serial, struct wl_surface *surface)
{
  struct input *input = data;

  input_remove_pointer_focus (input);
}

static void
pointer_handle_motion (void *data, struct wl_pointer *pointer,
    uint32_t time, wl_fixed_t sx_w, wl_fixed_t sy_w)
{
  struct input *input = data;
  GstWlWindow *window = input->pointer_focus;

  if (!window)
    return;

  if (input->grab)
    wl_shell_surface_move (input->grab->shell_surface, input->seat,
        input->display->serial);

}

static void
pointer_handle_button (void *data, struct wl_pointer *pointer, uint32_t serial,
    uint32_t time, uint32_t button, uint32_t state_w)
{
  struct input *input = data;
  enum wl_pointer_button_state state = state_w;
  input->display->serial = serial;

  if (button == BTN_LEFT) {
    if (state == WL_POINTER_BUTTON_STATE_PRESSED)
      input_grab (input, input->pointer_focus);

    if (input->grab && state == WL_POINTER_BUTTON_STATE_RELEASED)
      input_ungrab (input);
  }

  if (input->grab)
    wl_shell_surface_move (input->grab->shell_surface, input->seat,
        input->display->serial);
}

static void
pointer_handle_axis (void *data, struct wl_pointer *pointer,
    uint32_t time, uint32_t axis, wl_fixed_t value)
{
}

static const struct wl_pointer_listener pointer_listener = {
  pointer_handle_enter,
  pointer_handle_leave,
  pointer_handle_motion,
  pointer_handle_button,
  pointer_handle_axis,
};

static void
touch_handle_down (void *data, struct wl_touch *wl_touch,
    uint32_t serial, uint32_t time, struct wl_surface *surface,
    int32_t id, wl_fixed_t x_w, wl_fixed_t y_w)
{
  struct input *input = data;
  struct touch_point *tp;

  input->display->serial = serial;
  input->touch_focus = wl_surface_get_user_data (surface);
  if (!input->touch_focus) {
    return;
  }

  tp = malloc (sizeof *tp);
  if (tp) {
    tp->id = id;
    wl_list_insert (&input->touch_point_list, &tp->link);
    wl_shell_surface_move (input->touch_focus->shell_surface, input->seat,
        serial);
  }
}

static void
touch_handle_motion (void *data, struct wl_touch *wl_touch,
    uint32_t time, int32_t id, wl_fixed_t x_w, wl_fixed_t y_w)
{
  struct input *input = data;
  struct touch_point *tp;


  if (!input->touch_focus) {
    return;
  }
  wl_list_for_each (tp, &input->touch_point_list, link) {
    if (tp->id != id)
      continue;

    wl_shell_surface_move (input->touch_focus->shell_surface, input->seat,
        input->display->serial);

    return;
  }
}

static void
touch_handle_frame (void *data, struct wl_touch *wl_touch)
{
}

static void
touch_handle_cancel (void *data, struct wl_touch *wl_touch)
{
}

static void
touch_handle_up (void *data, struct wl_touch *wl_touch,
    uint32_t serial, uint32_t time, int32_t id)
{
  struct input *input = data;
  struct touch_point *tp, *tmp;

  if (!input->touch_focus) {
    return;
  }

  wl_list_for_each_safe (tp, tmp, &input->touch_point_list, link) {
    if (tp->id != id)
      continue;

    wl_list_remove (&tp->link);
    free (tp);

    return;
  }
}

static const struct wl_touch_listener touch_listener = {
  touch_handle_down,
  touch_handle_up,
  touch_handle_motion,
  touch_handle_frame,
  touch_handle_cancel,
};


static void
seat_handle_capabilities (void *data, struct wl_seat *seat,
    enum wl_seat_capability caps)
{
  struct input *input = data;

  if ((caps & WL_SEAT_CAPABILITY_POINTER) && !input->pointer) {
    input->pointer = wl_seat_get_pointer (seat);
    wl_pointer_set_user_data (input->pointer, input);
    wl_pointer_add_listener (input->pointer, &pointer_listener, input);
  } else if (!(caps & WL_SEAT_CAPABILITY_POINTER) && input->pointer) {
    wl_pointer_destroy (input->pointer);
    input->pointer = NULL;
  }

  if ((caps & WL_SEAT_CAPABILITY_TOUCH) && !input->touch) {
    input->touch = wl_seat_get_touch (seat);
    wl_touch_set_user_data (input->touch, input);
    wl_touch_add_listener (input->touch, &touch_listener, input);
  } else if (!(caps & WL_SEAT_CAPABILITY_TOUCH) && input->touch) {
    wl_touch_destroy (input->touch);
    input->touch = NULL;
  }
}

static void
seat_handle_name (void *data, struct wl_seat *seat, const char *name)
{
 
}

static const struct wl_seat_listener seat_listener = {
  seat_handle_capabilities,
  seat_handle_name
};

static void
display_add_input (GstWlDisplay *d, uint32_t id)
{
  struct input *input;

  input = calloc (1, sizeof (*input));
  if (input == NULL) {
    fprintf (stderr, "%s: out of memory\n", "gst-wayland-sink");
    exit (EXIT_FAILURE);
  }
  input->display = d;
  input->seat = wl_registry_bind (d->registry, id, &wl_seat_interface,
      MAX (d->seat_version, 3));
  input->touch_focus = NULL;
  input->pointer_focus = NULL;
  wl_list_init (&input->touch_point_list);
  wl_list_insert (d->input_list.prev, &input->link);

  wl_seat_add_listener (input->seat, &seat_listener, input);
  wl_seat_set_user_data (input->seat, input);

}


static void
registry_handle_global (void *data, struct wl_registry *registry,
    uint32_t id, const char *interface, uint32_t version)
{
  GstWlDisplay *self = data;

  if (g_strcmp0 (interface, "wl_compositor") == 0) {
    self->compositor = wl_registry_bind (registry, id, &wl_compositor_interface,
        MIN (version, 3));
  } else if (g_strcmp0 (interface, "wl_subcompositor") == 0) {
    self->subcompositor =
        wl_registry_bind (registry, id, &wl_subcompositor_interface, 1);
  } else if (g_strcmp0 (interface, "wl_shell") == 0) {
    self->shell = wl_registry_bind (registry, id, &wl_shell_interface, 1);
  } else if (g_strcmp0 (interface, "wl_shm") == 0) {
    self->shm = wl_registry_bind (registry, id, &wl_shm_interface, 1);
    wl_shm_add_listener (self->shm, &shm_listener, self);
  } else if (g_strcmp0 (interface, "wl_drm") == 0) {
    self->drm = wl_registry_bind (registry, id, &wl_drm_interface, 1);
    wl_drm_add_listener (self->drm, &drm_listener, self);
  } else if (g_strcmp0 (interface, "wl_seat") == 0) {
    self->seat_version = version;
    display_add_input (self, id);
  } else if (g_strcmp0 (interface, "wl_scaler") == 0) {
    self->scaler = wl_registry_bind (registry, id, &wl_scaler_interface, 2);
  }
}

static const struct wl_registry_listener registry_listener = {
  registry_handle_global
};

static gpointer
gst_wl_display_thread_run (gpointer data)
{
  GstWlDisplay *self = data;
  GstPollFD pollfd = GST_POLL_FD_INIT;

  pollfd.fd = wl_display_get_fd (self->display);
  gst_poll_add_fd (self->wl_fd_poll, &pollfd);
  gst_poll_fd_ctl_read (self->wl_fd_poll, &pollfd, TRUE);

  /* main loop */
  while (1) {
    while (wl_display_prepare_read_queue (self->display, self->queue) != 0)
      wl_display_dispatch_queue_pending (self->display, self->queue);
    wl_display_flush (self->display);

    if (gst_poll_wait (self->wl_fd_poll, GST_CLOCK_TIME_NONE) < 0) {
      gboolean normal = (errno == EBUSY);
      wl_display_cancel_read (self->display);
      if (normal)
        break;
      else
        goto error;
    } else {
      wl_display_read_events (self->display);
      wl_display_dispatch_queue_pending (self->display, self->queue);
    }
  }

  return NULL;

error:
  GST_ERROR ("Error communicating with the wayland server");
  return NULL;
}

GstWlDisplay *
gst_wl_display_new (const gchar * name, GError ** error)
{
  struct wl_display *display;

  display = wl_display_connect (name);

  if (!display) {
    *error = g_error_new (g_quark_from_static_string ("GstWlDisplay"), 0,
        "Failed to connect to the wayland display '%s'",
        name ? name : "(default)");
    return NULL;
  } else {
    return gst_wl_display_new_existing (display, TRUE, error);
  }
}

GstWlDisplay *
gst_wl_display_new_existing (struct wl_display * display,
    gboolean take_ownership, GError ** error)
{
  GstWlDisplay *self;
  GError *err = NULL;
  gint i;

  g_return_val_if_fail (display != NULL, NULL);

  self = g_object_new (GST_TYPE_WL_DISPLAY, NULL);
  self->display = display;
  self->own_display = take_ownership;

  self->queue = wl_display_create_queue (self->display);
  wl_list_init (&self->input_list);
  self->registry = wl_display_get_registry (self->display);
  wl_proxy_set_queue ((struct wl_proxy *) self->registry, self->queue);
  wl_registry_add_listener (self->registry, &registry_listener, self);

  /* we need exactly 2 roundtrips to discover global objects and their state */
  for (i = 0; i < 2; i++) {
    if (gst_wl_display_roundtrip (self) < 0) {
      *error = g_error_new (g_quark_from_static_string ("GstWlDisplay"), 0,
          "Error communicating with the wayland display");
      g_object_unref (self);
      return NULL;
    }
  }

  /* verify we got all the required interfaces */
#define VERIFY_INTERFACE_EXISTS(var, interface) \
  if (!self->var) { \
    g_set_error (error, g_quark_from_static_string ("GstWlDisplay"), 0, \
        "Could not bind to " interface ". Either it is not implemented in " \
        "the compositor, or the implemented version doesn't match"); \
    g_object_unref (self); \
    return NULL; \
  }

  VERIFY_INTERFACE_EXISTS (compositor, "wl_compositor");
  VERIFY_INTERFACE_EXISTS (subcompositor, "wl_subcompositor");
  VERIFY_INTERFACE_EXISTS (shell, "wl_shell");
  VERIFY_INTERFACE_EXISTS (shm, "wl_shm");
  VERIFY_INTERFACE_EXISTS (drm, "wl_drm");
  VERIFY_INTERFACE_EXISTS (scaler, "wl_scaler");

#undef VERIFY_INTERFACE_EXISTS

  self->thread = g_thread_try_new ("GstWlDisplay", gst_wl_display_thread_run,
      self, &err);
  if (err) {
    g_propagate_prefixed_error (error, err,
        "Failed to start thread for the display's events");
    g_object_unref (self);
    return NULL;
  }

  return self;
}

void
gst_wl_display_register_buffer (GstWlDisplay * self, gpointer buf)
{
  g_assert (!self->shutting_down);

  GST_TRACE_OBJECT (self, "registering GstWlBuffer %p", buf);

  g_mutex_lock (&self->buffers_mutex);
  g_hash_table_add (self->buffers, buf);
  g_mutex_unlock (&self->buffers_mutex);
}

void
gst_wl_display_unregister_buffer (GstWlDisplay * self, gpointer buf)
{
  GST_TRACE_OBJECT (self, "unregistering GstWlBuffer %p", buf);

  g_mutex_lock (&self->buffers_mutex);
  if (G_LIKELY (!self->shutting_down))
    g_hash_table_remove (self->buffers, buf);
  g_mutex_unlock (&self->buffers_mutex);
}
