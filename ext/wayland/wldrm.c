#include "wldisplay-wlwindow-wlbuffer.h"
#include <gst/drm/gstdrmallocator.h>
#include "wayland-drm-client-protocol.h"
#include <omap_drm.h>
#include <omap_drmif.h>
#include <wayland-client.h>

GST_DEBUG_CATEGORY_EXTERN (gstwayland_debug);
#define GST_CAT_DEFAULT gstwayland_debug


struct wl_buffer *
gst_wl_drm_memory_construct_wl_buffer (GstMemory * mem, GstWlDisplay * display,
    const GstVideoInfo * info)
{
  gint video_width = GST_VIDEO_INFO_WIDTH (info);
  gint video_height = GST_VIDEO_INFO_HEIGHT (info);
  GstVideoFormat format = GST_VIDEO_INFO_FORMAT (info);
  int fd = -1;
  struct omap_bo *bo;
  struct wl_buffer *buffer;
  uint32_t fourcc;
  uint32_t name;
  /* note: wayland and mesa use the terminology:
   *    stride - rowstride in bytes
   *    pitch  - rowstride in pixels
   */
  uint32_t strides[3] = {
    GST_ROUND_UP_4 (video_width), 0, 0,
  };
  uint32_t offsets[3] = {
    0, strides[0] * video_height, 0
  };

  if (format == GST_VIDEO_FORMAT_NV12)
  {
    /* NV12 */
    fourcc = GST_MAKE_FOURCC ('N', 'V', '1', '2');
    strides[1] = GST_ROUND_UP_4 (video_width);
  }
  else if(format == GST_VIDEO_FORMAT_I420)
  {
    /* YUV420 */
    fourcc = GST_MAKE_FOURCC ('Y', 'U', '1', '2');
    strides[1] = strides[2] = GST_ROUND_UP_4 (video_width/2);
    offsets[2] = offsets[1] + strides[1] * video_height/2;
  }
  else
  {

    GST_DEBUG ("Unsupported video format: %d", format);
    /*
     * There are two xRGB frames with width and height = 1 required in the begining of a video stream.
     * If we consider them as errot, then it will case libwayland-clent.so crashes
     * due to invalid error handling.
     * Consider them as NV12 until we can figure out a better solution
     */
    fourcc = GST_MAKE_FOURCC ('N', 'V', '1', '2');
    strides[1] = GST_ROUND_UP_4 (video_width);
  }

  fd = gst_fd_memory_get_fd (mem);

  if (fd < 0 ) {
    GST_DEBUG ("Invalid fd");
    return NULL;
  }

  bo = omap_bo_from_dmabuf (display->dev, fd);

  struct drm_gem_flink req = {
                  .handle = omap_bo_handle(bo),
  };

  int ret;
  ret = drmIoctl(display->fd, DRM_IOCTL_GEM_FLINK, &req);
  if (ret) {
    GST_DEBUG ("could not get name, DRM_IOCTL_GEM_FLINK returned %d", ret);
    return NULL;
  }

  name = req.name;

 GST_LOG ("width = %d , height = %d , fourcc = %d ",  video_width, video_height, fourcc );
 buffer = wl_drm_create_planar_buffer (display->drm, name,
      video_width, video_height, fourcc,
      offsets[0], strides[0],
      offsets[1], strides[1],
      offsets[2], strides[2]);

  GST_DEBUG ("create planar buffer: %p (name=%d)",
      buffer, name);

  return buffer;
}

