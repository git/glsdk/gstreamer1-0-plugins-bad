/* GStreamer Wayland video sink
 *
 * Copyright (C) 2014 Collabora Ltd.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA.
 */

#ifndef __GST_WL_DISPLAY_WL_WINDOW_H__
#define __GST_WL_DISPLAY_WL_WINDOW_H__

#include <gst/gst.h>
#include <stdlib.h>
#include <stdio.h>
#include <wayland-client.h>
#include "scaler-client-protocol.h"
#include <gst/video/video.h>
#include <gst/video/gstvideometa.h>

G_BEGIN_DECLS

#define GST_TYPE_WL_DISPLAY                  (gst_wl_display_get_type ())
#define GST_WL_DISPLAY(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_WL_DISPLAY, GstWlDisplay))
#define GST_IS_WL_DISPLAY(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_WL_DISPLAY))
#define GST_WL_DISPLAY_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), GST_TYPE_WL_DISPLAY, GstWlDisplayClass))
#define GST_IS_WL_DISPLAY_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_TYPE_WL_DISPLAY))
#define GST_WL_DISPLAY_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_TYPE_WL_DISPLAY, GstWlDisplayClass))

#define GST_TYPE_WL_WINDOW                  (gst_wl_window_get_type ())
#define GST_WL_WINDOW(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_WL_WINDOW, GstWlWindow))
#define GST_IS_WL_WINDOW(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_WL_WINDOW))
#define GST_WL_WINDOW_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), GST_TYPE_WL_WINDOW, GstWlWindowClass))
#define GST_IS_WL_WINDOW_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_TYPE_WL_WINDOW))
#define GST_WL_WINDOW_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_TYPE_WL_WINDOW, GstWlWindowClass))

#define GST_TYPE_WL_BUFFER                  (gst_wl_buffer_get_type ())
#define GST_WL_BUFFER(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_WL_BUFFER, GstWlBuffer))
#define GST_IS_WL_BUFFER(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_WL_BUFFER))
#define GST_WL_BUFFER_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), GST_TYPE_WL_BUFFER, GstWlBufferClass))
#define GST_IS_WL_BUFFER_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_TYPE_WL_BUFFER))
#define GST_WL_BUFFER_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_TYPE_WL_BUFFER, GstWlBufferClass))

typedef struct _GstWlBuffer GstWlBuffer;
typedef struct _GstWlBufferClass GstWlBufferClass;

typedef struct _GstWlWindow GstWlWindow;
typedef struct _GstWlWindowClass GstWlWindowClass;

typedef struct _GstWlDisplay GstWlDisplay;
typedef struct _GstWlDisplayClass GstWlDisplayClass;

struct _GstWlBuffer
{
  GObject parent_instance;

  struct wl_buffer * wlbuffer;
  GstBuffer *gstbuffer;

  GstWlDisplay *display;

  gboolean used_by_compositor;
};

struct _GstWlBufferClass
{
  GObjectClass parent_class;
};

GType gst_wl_buffer_get_type (void);

GstWlBuffer * gst_buffer_add_wl_buffer (GstBuffer * gstbuffer,
    struct wl_buffer * wlbuffer, GstWlDisplay * display);
GstWlBuffer * gst_buffer_get_wl_buffer (GstBuffer * gstbuffer);

void gst_wl_buffer_force_release_and_unref (GstWlBuffer * self);

void gst_wl_buffer_attach (GstWlBuffer * self, struct wl_surface *surface);


struct touch_point
{
  int32_t id;
  struct wl_list link;
};
       
struct input
{
  GstWlDisplay *display;
  struct wl_seat *seat;
  struct wl_pointer *pointer;
  struct wl_touch *touch;
  struct wl_list touch_point_list;
  GstWlWindow *pointer_focus;
  GstWlWindow *touch_focus;
  struct wl_list link; 
  GstWlWindow *grab;      
};


struct _GstWlWindow
{
  GObject parent_instance;

  GstWlDisplay *display;
  struct wl_surface *area_surface;
  struct wl_subsurface *area_subsurface;
  struct wl_viewport *area_viewport;
  struct wl_surface *video_surface;
  struct wl_subsurface *video_subsurface;
  struct wl_viewport *video_viewport;
  struct wl_shell_surface *shell_surface;

  /* the size and position of the area_(sub)surface */
  GstVideoRectangle render_rectangle;
  /* the size of the video in the buffers */
  gint video_width, video_height;
  /* the size of the video_(sub)surface */
  gint surface_width, surface_height;
};

struct _GstWlWindowClass
{
  GObjectClass parent_class;
};

GType gst_wl_window_get_type (void);

GstWlWindow *gst_wl_window_new_toplevel (GstWlDisplay * display,
        const GstVideoInfo * info);
GstWlWindow *gst_wl_window_new_in_surface (GstWlDisplay * display,
        struct wl_surface * parent);

GstWlDisplay *gst_wl_window_get_display (GstWlWindow * window);
struct wl_surface *gst_wl_window_get_wl_surface (GstWlWindow * window);
gboolean gst_wl_window_is_toplevel (GstWlWindow *window);

void gst_wl_window_render (GstWlWindow * window, GstWlBuffer * buffer,
        const GstVideoInfo * info);
void gst_wl_window_set_render_rectangle (GstWlWindow * window, gint x, gint y,
        gint w, gint h);


struct _GstWlDisplay
{
  GObject parent_instance;

  /* public objects */
  struct wl_display *display;
  struct wl_event_queue *queue;

  /* globals */
  struct wl_registry *registry;
  struct wl_compositor *compositor;
  struct wl_subcompositor *subcompositor;
  struct wl_shell *shell;
  struct wl_shm *shm;
  struct wl_drm *drm;
  struct wl_scaler *scaler;
  GArray *formats;

  /* private */
  gboolean own_display;
  GThread *thread;
  GstPoll *wl_fd_poll;

  GMutex buffers_mutex;
  GHashTable *buffers;
  gboolean shutting_down;

  /* the drm device.. needed for sharing direct-render buffers..
   * TODO nothing about this should really be omapdrm specific.  But some
   * of the code, like hashtable of imported buffers in libdrm_omap should
   * be refactored out into some generic libdrm code..
   */
  struct omap_device *dev;
  int fd;
  int authenticated;
  gboolean use_drm;

  struct wl_list input_list;
  int seat_version;
  uint32_t serial;

  GstVideoCropMeta *crop;
};

struct _GstWlDisplayClass
{
  GObjectClass parent_class;
};

GType gst_wl_display_get_type (void);

GstWlDisplay *gst_wl_display_new (const gchar * name, GError ** error);
GstWlDisplay *gst_wl_display_new_existing (struct wl_display * display,
    gboolean take_ownership, GError ** error);

/* see wlbuffer.c for explanation */
void gst_wl_display_register_buffer (GstWlDisplay * self, gpointer buf);
void gst_wl_display_unregister_buffer (GstWlDisplay * self, gpointer buf);

G_END_DECLS

#endif /* __GST_WL_DISPLAY_WL_WINDOW_H__ */
